import {
	LOAD_DATA_REQUEST,
	LOAD_DATA_RESPONSE,
	UPDATE_DAY_ITEM,
} from './constants';

const CONTENT = `"We’re mistaken when we think that technology just automatically improves...it only improves if a lot of people work very hard to make it better."
  — Elon Musk, #TED2017 Catch Elon's full TED...
  — Elon Musk, #TED2017 Catch Elon's full TED...
  — Elon Musk, #TED2017 Catch Elon's full TED...
  — Elon Musk, #TED2017 Catch Elon's full TED...`;

export function loadData() {
	return function(dispatch) {
		dispatch({ type: LOAD_DATA_REQUEST });
		return new Promise(resolve => setTimeout(resolve, 500)).then(_ => {
			const stat = {
				retweet: 129,
				profile: 2,
				like: 352,
				comment: 14,
			};
			const days = [
				{
					date: new Date(),
					slots: [
						{
							id: 1,
							time: '10:00',
							content: CONTENT,
							title: 'Elon Musk, #TED2017 ',
							checked: { twitter: true, google: true },
						},
						{ id: 2, time: '12:05', emptyText: 'Third slot' },
						{
							id: 3,
							time: '23:00',
							content: CONTENT,
							title: 'Elon Musk, #TED2017 ',
							checked: { facebook: true },
						},
						{ id: 4 },
						{ id: 5 },
					],
				},
			];

			dispatch({ type: LOAD_DATA_RESPONSE, stat, days });
		});
	};
}

export function updateDayItem(date, slot) {
	return { type: UPDATE_DAY_ITEM, date, slot };
}
