import { createStore, compose as composeOrig, applyMiddleware } from 'redux';
import reducer from './reducer';
import thunk from 'redux-thunk';

const initialState = {
	content: {
		isLoad: undefined,
		data: {
			stat: {
				retweet: 0,
				profile: 0,
				like: 0,
				comment: 0,
			},
			days: [],
		},
	},
};

const compose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || composeOrig;

const middlewares = [thunk];
export const store = createStore(
	reducer,
	initialState,
	compose(applyMiddleware(...middlewares))
);
