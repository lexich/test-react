import {
	LOAD_DATA_REQUEST,
	LOAD_DATA_RESPONSE,
	UPDATE_DAY_ITEM,
} from './constants';
import * as R from 'ramda';
import dayjs from 'dayjs';

const updateDays = R.lensPath(['content', 'data', 'days']);

function diff(a1, a2) {
	const d = a2 - a1;
	if (!d) {
		return 0;
	}
	return d > 0 ? -1 : 1;
}

function comparatorDays(a1, a2) {
	const t1 = a1.date.getTime();
	const t2 = a2.date.getTime();
	return diff(t1, t2);
}

function comparatorSlots(a1, a2) {
	const t1 = /(\d{2}):(\d{2})/.exec(a1.time || '');
	const t2 = /(\d{2}):(\d{2})/.exec(a2.time || '');
	if (!t1 && !t2) {
		return 0;
	}
	if (!t1) {
		return 1;
	}
	if (!t2) {
		return -1;
	}
	const d = diff(t1[1], t2[1]);
	return d || diff(t1[2], t2[2]);
}

export default function(state, action) {
	switch (action.type) {
		case LOAD_DATA_REQUEST:
			return {
				content: {
					...state.content,
					isLoad: false,
				},
			};

		case LOAD_DATA_RESPONSE:
			return {
				content: {
					isLoad: true,
					data: {
						stat: action.stat,
						days: action.days,
					},
				},
			};

		case UPDATE_DAY_ITEM: {
			let newState = state;
			// clean state from old item
			const days = R.view(updateDays, newState).map(day => {
				const newSlots = day.slots.filter(slot => slot.id !== action.slot.id);
				if (newSlots.length === day.slots.length) {
					return day;
				}
				return { ...day, slots: newSlots };
			});
			newState = R.set(updateDays, days, newState);

			const existDay = days.find(
				day => dayjs(day.date).diff(action.date, 'days') === 0
			);
			if (existDay) {
				const newDays = days.map(day => {
					if (dayjs(day.date).diff(action.date, 'days') !== 0) {
						return day;
					}

					const newSlots = day.slots.concat(action.slot).sort(comparatorSlots);
					return { ...day, slots: newSlots };
				});
				return R.set(updateDays, newDays, newState);
			} else {
				const newDays = days
					.concat({
						date: action.date,
						slots: [action.slot],
					})
					.sort(comparatorDays);
				return R.set(updateDays, newDays, newState);
			}
		}

		default:
			return state;
	}
}
