import React, { Component } from 'react';
import Page from './components/Page/Page';

import { connect } from 'react-redux';
import { loadData, updateDayItem } from './redux/actions';
import PlanPage from './components/PlanPage/PlanPage';
import style from './App.module.css';

export class App extends Component {
	componentDidMount() {
		this.props.loadData();
	}
	render() {
		const { content } = this.props;
		return (
			<Page stat={content.isLoad ? content.data.stat : undefined}>
				<PlanPage
					className={style.content}
					classNameLoader={style.loader}
					content={content}
					onApply={this.props.onApply}
				/>
			</Page>
		);
	}
}

function select(state) {
	return { content: state.content };
}

function map2fn(dispatch) {
	return {
		onApply(date, content) {
			dispatch(updateDayItem(date, content));
		},
		loadData() {
			dispatch(loadData());
		},
	};
}

export default connect(
	select,
	map2fn
)(App);
