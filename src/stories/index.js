import React from 'react';
import { storiesOf } from '@storybook/react';
import Slot from '../components/Slot/Slot';
import Day from '../components/Day/Day';
import Editor from '../components/Editor/Editor';
import EditorModal from '../components/Editor/EditorModal';
import SmartDay from '../components/SmartDay/SmartDay';
import Page from '../components/Page/Page';

const CONTENT = `"We’re mistaken when we think that technology just automatically improves...it only improves if a lot of people work very hard to make it better."
  — Elon Musk, #TED2017 Catch Elon's full TED...
  — Elon Musk, #TED2017 Catch Elon's full TED...
  — Elon Musk, #TED2017 Catch Elon's full TED...
  — Elon Musk, #TED2017 Catch Elon's full TED...`;

const SLOTS = [
	{ id: 1, time: '10:00 am', content: CONTENT, title: 'Elon Musk, #TED2017 ' },
	{ id: 2, time: '23:05 pm', emptyText: 'Third slot' },
	{ id: 3 },
	{ id: 5, time: '10:00 am', content: CONTENT, title: 'Elon Musk, #TED2017 ' },
	{ id: 4 },
];

storiesOf('Slot', module).add('Default', () => <Slot id={1} />);

storiesOf('Day', module)
	.add('Default', () => (
		<Day dayName="Today" weekDay="Friday">
			<Slot id={1} time="10:00 am">
				{CONTENT}
			</Slot>
			<Slot id={2} time="23:05" emptyText="Third slot" />
			<Slot id={3} />
			<Slot id={4} />
		</Day>
	))
	.add('SmartDay', () => (
		<SmartDay
			dayName="Today"
			date={new Date()}
			weekDay="Friday"
			slots={SLOTS}
		/>
	))
	.add('SmartDay Multiline', () => (
		<div style={{ width: '500px' }}>
			<SmartDay
				dayName="Today"
				weekDay="Friday"
				date={new Date()}
				slots={SLOTS}
				onApply={() => {}}
			/>
		</div>
	));

storiesOf('Editor', module)
	.add('Default', () => (
		<Editor
			title="New post"
			content=""
			checked={{
				facebook: true,
				twitter: false,
				google: true,
				instagram: false,
			}}
			onClose={() => {}}
			onApply={() => {}}
		/>
	))
	.add('EditorModal', () => (
		<EditorModal
			title="New post"
			content=""
			onClose={() => {}}
			onApply={() => {}}
		/>
	));

storiesOf('Page', module)
	.add('Default', () => (
		<div style={{ position: 'relative', left: '-8px', top: '-8px' }}>
			<Page />
		</div>
	))
	.add('With Smart day', () => (
		<div style={{ position: 'relative', left: '-8px', top: '-8px' }}>
			<Page>
				<SmartDay
					dayName="Today"
					weekDay="Friday"
					date={new Date()}
					slots={SLOTS}
					onApply={() => {}}
				/>
				<SmartDay
					dayName="Today"
					weekDay="Friday"
					date={new Date()}
					slots={SLOTS}
					onApply={() => {}}
				/>
				<SmartDay
					dayName="Today"
					weekDay="Friday"
					date={new Date()}
					slots={SLOTS}
					onApply={() => {}}
				/>
				<SmartDay
					dayName="Today"
					weekDay="Friday"
					date={new Date()}
					slots={SLOTS}
					onApply={() => {}}
				/>
			</Page>
		</div>
	));
