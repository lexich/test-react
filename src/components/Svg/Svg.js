/* eslint-disable import/no-webpack-loader-syntax */

import Facebook from '-!svg-react-loader?name=Facebook!./facebook.svg';
import FacebookT from '-!svg-react-loader?name=FacebookT!./facebook-t.svg';

import Google from '-!svg-react-loader?name=Google!./google.svg';
import GoogleT from '-!svg-react-loader?name=GoogleT!./google-t.svg';

import Instagram from '-!svg-react-loader?name=Instagram!./instagram.svg';
import InstagramT from '-!svg-react-loader?name=InstagramT!./instagram-t.svg';

import Twitter from '-!svg-react-loader?name=Twitter!./twitter.svg';
import TwitterT from '-!svg-react-loader?name=TwitterT!./twitter-t.svg';

import Youtube from '-!svg-react-loader?name=Youtube!./youtube.svg';

export default {
	Facebook,
	FacebookT,
	Google,
	GoogleT,
	Instagram,
	InstagramT,
	Twitter,
	TwitterT,
	Youtube,
};
