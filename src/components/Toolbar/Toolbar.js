import React from 'react';
import Icon from '../Icon/Icon';
import style from './Toolbar.module.css';
import C from 'classnames';

export default function Toolbar(props) {
	return (
		<ul className={C(style.main, props.className)}>
			<li className={style.item}>
				<Icon name="Profile" />{' '}
				<span className={style.text}>{props.profile}</span>
			</li>
			<li className={style.item}>
				<Icon name="Retweet" />{' '}
				<span className={style.text}>{props.retweet}</span>
			</li>
			<li className={style.item}>
				<Icon name="Like" /> <span className={style.text}>{props.like}</span>
			</li>
			<li className={style.item}>
				<Icon name="Comment" />{' '}
				<span className={style.text}>{props.comment}</span>
			</li>
		</ul>
	);
}
