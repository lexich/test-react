import React from 'react';
import style from './Loader.module.css';
import C from 'classnames';
import PropTypes from 'prop-types';

export default function Loader({ className }) {
	return (
		<div className={C(style.main, className)}>
			<div className={C(style.item, style.item1)} />
			<div className={C(style.item, style.item2)} />
			<div className={C(style.item, style.item3)} />
		</div>
	);
}

Loader.propTypes = {
	className: PropTypes.string,
};
