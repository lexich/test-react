import React from 'react';
import C from 'classnames';
import style from './Page.module.css';
import Modal from '../Modal/Modal';
import Toolbar from '../Toolbar/Toolbar';
import PropTypes from 'prop-types';

class Page extends React.Component {
	state = {
		mobileMenu: false,
	};

	static propTypes = {
		menu: PropTypes.any,
		children: PropTypes.any,
		stat: PropTypes.shape({
			retweet: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
				.isRequired,
			profile: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
				.isRequired,
			like: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
				.isRequired,
			comment: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
				.isRequired,
		}),
	};

	render() {
		const { stat } = this.props;
		const { mobileMenu } = this.state;
		const Overlay = !mobileMenu ? null : (
			<Modal>
				<div className={style.overlay} onClick={this.onCloseMenu} />
			</Modal>
		);
		const StatToolbar = !stat ? null : (
			<Toolbar
				className={style.statistic}
				retweet={stat.retweet}
				profile={stat.profile}
				like={stat.like}
				comment={stat.comment}
			/>
		);
		return (
			<div className={style.main}>
				<div
					className={C(style.menu, { [style.active]: this.state.mobileMenu })}
					onClick={this.onCloseMenu}
				>
					{this.props.menu}
				</div>
				<div className={style.toolbar}>
					<button className={style.menubtn} onClick={this.onOpenMenu}>
						&#9776;
					</button>
				</div>
				<div className={style.body}>
					{StatToolbar}
					<div className={style.content}>{this.props.children}</div>
				</div>
				{Overlay}
			</div>
		);
	}

	onOpenMenu = () => this.setState({ mobileMenu: true });
	onCloseMenu = () => this.setState({ mobileMenu: false });
}

export default Page;
