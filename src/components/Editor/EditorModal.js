import React from 'react';
import Modal from '../Modal/Modal';
import Editor from './Editor';
import style from './EditorModal.module.css';
import C from 'classnames';

export default class EditorModal extends React.Component {
	state = {
		out: false,
	};
	render() {
		const { className, onClose, onApply, ...props } = this.props;
		return (
			<Modal>
				<Editor
					className={C(style.main, className, { [style.out]: this.state.out })}
					classNameBackground={style.background}
					classNameModal={style.modal}
					onClose={this.onClose}
					onApply={this.onApply}
					{...props}
				/>
				<div
					className={C(style.overlay, { [style.out]: this.state.out })}
					onClick={this.onClose}
				/>
			</Modal>
		);
	}

	onApply = (...args) => {
		if (this.props.onApply) {
			this.props.onApply(...args);
		}
		this.onClose();
	};

	onClose = () => {
		const { onClose } = this.props;
		this.setState({ out: true }, () => {
			onClose && setTimeout(onClose, 500);
		});
	};
}

EditorModal.propTypes = Editor.propTypes; // eslint-disable-line
