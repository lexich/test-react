import React from 'react';
import style from './Close.module.css';
import C from 'classnames';
export default function Close({ className, ...props }) {
	return (
		<div className={C(style.container, className)} {...props}>
			<div className={style.leftright} />
			<div className={style.rightleft} />
			<label className={style.close}>close</label>
		</div>
	);
}
