import React from 'react';
import C from 'classnames';
/* eslint-disable import/no-webpack-loader-syntax */

import PropTypes from 'prop-types';
import style from './Editor.module.css';
import Calendar from 'react-calendar';
import Dropdown, { MenuItem } from '@trendmicro/react-dropdown';
import '@trendmicro/react-dropdown/dist/react-dropdown.css';
import Close from './Close';
import dayjs from 'dayjs';
import Svg from '../Svg/Svg';
import Checkbox from '../Checkbox/Checkbox';

function range(from, to) {
	const len = to - from;
	if (len < 0) {
		return [];
	}
	const res = new Array(len);

	for (let i = 0; i < len; i++) {
		res[i] = from + i;
	}
	return res;
}
const HOURS = range(0, 23);
const MUNUTES = range(0, 59);

function format(n) {
	return n > 9 ? '' + n : '0' + n;
}

export default class Editor extends React.Component {
	static propTypes = {
		className: PropTypes.string,
		classNameBackground: PropTypes.string,
		onClose: PropTypes.func.isRequired,
		onApply: PropTypes.func.isRequired,
		title: PropTypes.string,
		content: PropTypes.string,
		date: PropTypes.instanceOf(Date),
		checked: PropTypes.shape({
			facebook: PropTypes.bool,
			twitter: PropTypes.bool,
			instagram: PropTypes.bool,
			google: PropTypes.bool,
		}),
	};

	static initState(props) {
		const title = props.title || '';
		const content = props.content || '';
		const date = props.date || new Date();
		const ret = /(\d{2}):(\d{2})/.exec(props.time || '');

		const [hour, minutes] = ret
			? [+RegExp.$1, +RegExp.$2]
			: (function(d) {
					return [d.getHours(), d.getMinutes()];
			  })(new Date());
		const checked = {
			facebook: false,
			twitter: false,
			instagram: false,
			google: false,
		};
		Object.assign(checked, props.checked);

		return { title, content, date, hour, minutes, checked };
	}

	state = Editor.initState(this.props);

	Hours = HOURS.map(val => (
		<MenuItem
			key={`h${val}`}
			eventKey={val}
			onSelect={(...args) => this.onSelectHours(val, ...args)}
		>
			{val}
		</MenuItem>
	));
	Minutes = MUNUTES.map(val => (
		<MenuItem
			key={`m${val}`}
			eventKey={val}
			onSelect={(...args) => this.onSelectMinutes(val, ...args)}
		>
			{val}
		</MenuItem>
	));

	render() {
		const {
			className,
			classNameBackground,
			classNameModal,
			onClose,
		} = this.props;
		const { checked } = this.state;
		return (
			<div className={C(className, style.main)}>
				<div className={C(classNameBackground, style.mainBackground)}>
					<div className={C(classNameModal, style.mainModal)}>
						<Close className={style.close} onClick={onClose} />
						<input
							className={style.posttitle}
							value={this.state.title}
							placeholder="New Title"
							onChange={this.onTitleChange}
						/>

						<div className={style.datetoolbar}>
							When to publish:
							<Dropdown className={style.dropdown_calendar}>
								<Dropdown.Toggle noCaret className={style.btn}>
									{dayjs(this.state.date).format('MMMM, DD, ddd')}
									<span className={style.caret} />
								</Dropdown.Toggle>
								<Dropdown.MenuWrapper>
									<Calendar
										onChange={this.onChangeDay}
										value={this.state.date}
										locale="en"
									/>
								</Dropdown.MenuWrapper>
							</Dropdown>
							<div className={style.timeblock}>
								at
								<Dropdown className={C(style.dropdown_time, style.first)}>
									<Dropdown.Toggle noCaret className={style.btn}>
										{format(this.state.hour)}
										<span className={style.caret} />
									</Dropdown.Toggle>
									<Dropdown.MenuWrapper
										style={{
											position: 'absolute',
											maxHeight: '200px',
											display: 'block',
											overflowY: 'auto',
										}}
									>
										<Dropdown.Menu>{this.Hours}</Dropdown.Menu>
									</Dropdown.MenuWrapper>
								</Dropdown>
								<Dropdown className={style.dropdown_time}>
									<Dropdown.Toggle noCaret className={style.btn}>
										{format(this.state.minutes)}
										<span className={style.caret} />
									</Dropdown.Toggle>
									<Dropdown.MenuWrapper
										style={{
											position: 'absolute',
											maxHeight: '200px',
											display: 'block',
											overflowY: 'auto',
										}}
									>
										<Dropdown.Menu>{this.Minutes}</Dropdown.Menu>
									</Dropdown.MenuWrapper>
								</Dropdown>
							</div>
						</div>

						<div className={style.editarea}>
							<div className={style.social}>
								<Checkbox
									onChange={this.onCheckFacebook}
									className={style.socialIconLabel}
									defaultChecked={checked.facebook}
								>
									<Svg.FacebookT className={style.socialIcon} />
								</Checkbox>
								<Checkbox
									onChange={this.onCheckTwitter}
									className={style.socialIconLabel}
									defaultChecked={checked.twitter}
								>
									<Svg.TwitterT className={style.socialIcon} />
								</Checkbox>
								<Checkbox
									onChange={this.onCheckGoogle}
									className={style.socialIconLabel}
									defaultChecked={checked.google}
								>
									<Svg.GoogleT className={style.socialIcon} />
								</Checkbox>
								<Checkbox
									onChange={this.onCheckInstragram}
									className={style.socialIconLabel}
									defaultChecked={checked.instagram}
								>
									<Svg.InstagramT className={style.socialIcon} />
								</Checkbox>
							</div>
							<textarea
								className={style.textarea}
								value={this.state.content}
								placeholder="Text and links..."
								onChange={this.onContentChange}
							/>
							<div className={style.buttonarea}>
								<button
									className={style.button_draft}
									disabled={!this.state.content}
									onClick={this.onDraft}
								>
									Save as Draft
								</button>
								<button
									className={style.button_save}
									onClick={this.onSave}
									disabled={!this.state.content}
								>
									Shedule Post
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}

	checkBuilder = name => event => {
		const { checked } = event.target;
		this.setState(state => {
			const val = { ...state.checked, [name]: checked };
			return { ...state, checked: val };
		});
	};

	onCheckTwitter = this.checkBuilder('twitter');
	onCheckGoogle = this.checkBuilder('google');
	onCheckInstragram = this.checkBuilder('instagram');
	onCheckFacebook = this.checkBuilder('facebook');

	onSelectHours = hour => this.setState({ hour });

	onSelectMinutes = minutes => this.setState({ minutes });

	onChangeDay = date => {
		this.setState({ date });
	};

	onApply(draft) {
		const { title, content, hour, minutes, checked, date } = this.state;
		const time = `${format(hour)}:${format(minutes)}`;
		this.props.onApply(
			{
				title,
				content,
				time,
				checked,
			},
			date,
			draft
		);
	}

	onDraft = () => this.onApply(true);

	onSave = () => this.onApply(false);

	onTitleChange = e => this.setState({ title: e.target.value });

	onContentChange = e => this.setState({ content: e.target.value });
}
