/* eslint-disable import/no-webpack-loader-syntax */

import React from 'react';
import Profile from '-!svg-react-loader?name=Profile!./profile.svg';
import Retweet from '-!svg-react-loader?name=Profile!./retweet.svg';
import Like from '-!svg-react-loader?name=Profile!./like.svg';
import Comment from '-!svg-react-loader?name=Profile!./comment.svg';
export const P = { Profile, Retweet, Like, Comment };

export default class Icon extends React.Component {
	render() {
		const { name, ...params } = this.props;
		const Icon = P[name];
		return !Icon ? null : <Icon {...params} />;
	}
}
