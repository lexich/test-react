import React from 'react';
import style from './Day.module.css';
import C from 'classnames';
import PropTypes from 'prop-types';

export default function Day(props) {
	return (
		<div className={C(props.className, style.main)}>
			<h1 className={style.title}>
				{props.dayName}
				<span className={style.subtitle}>{props.weekDay}</span>
			</h1>
			<div className={style.content}>{props.children}</div>
		</div>
	);
}

Day.propTypes = {
	className: PropTypes.string,
	dayName: PropTypes.string.isRequired,
	weekDay: PropTypes.string.isRequired,
	children: PropTypes.any,
};
