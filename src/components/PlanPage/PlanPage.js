import React from 'react';
import SmartDay from '../SmartDay/SmartDay';
import Loader from '../Loader/Loader';
import PropTypes from 'prop-types';

export default class PlanPage extends React.Component {
	static propTypes = {
		className: PropTypes.string,
		classNameLoader: PropTypes.string,
		content: PropTypes.shape({
			isLoad: PropTypes.bool,
			data: PropTypes.arrayOf(
				PropTypes.shape({
					date: PropTypes.instanceOf(Date).isRequired,
					slots: PropTypes.array.isRequired,
				})
			).isRequired,
		}).isRequired,
		onApply: PropTypes.func.isRequired,
	};

	render() {
		const { className, classNameLoader, content } = this.props;
		return !content.isLoad ? (
			<Loader className={classNameLoader} />
		) : (
			<div className={className}>
				{content.data.days.map(day => (
					<SmartDay
						key={day.date.valueOf()}
						date={day.date}
						slots={day.slots}
						onApply={this.props.onApply}
					/>
				))}
			</div>
		);
	}
}
