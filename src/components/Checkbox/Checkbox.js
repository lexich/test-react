import React from 'react';
import style from './Checkbox.module.css';
import C from 'classnames';

export default function Checkbox({ children, className, ...props }) {
	return (
		<label className={C(style.label, className)}>
			<input className={style.input} type="checkbox" {...props} />
			<div className={style.content}>{children}</div>
		</label>
	);
}
