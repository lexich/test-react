import React from 'react';
import style from './Slot.module.css';
import C from 'classnames';
import PropTypes from 'prop-types';
import Svg from '../Svg/Svg';
export default class Slot extends React.Component {
	state = {
		ispushed: false,
	};
	render() {
		const { children, time, emptyText, className } = this.props;
		const Body = !!children ? (
			this.renderContent()
		) : (
			<div
				className={style.empty}
				onClick={this.onClick}
				onMouseDown={this.onMouseDown}
				onMouseUp={this.onMouseUp}
				onMouseLeave={this.onMouseUp}
			>
				{!time ? '' : <span className={style.emptyTime}>{time}</span>}
				{emptyText || 'Schedule post on this day'}
			</div>
		);

		return (
			<div
				className={C(style.main, className, {
					[style.hasdata]: !!children,
					[style.pushed]: this.state.ispushed,
				})}
			>
				{Body}
			</div>
		);
	}

	renderContent() {
		const { checked, time, children, draft } = this.props;
		const c = checked || {};
		return (
			<div className={style.maincontent}>
				<div className={style.icons}>
					<Svg.Facebook
						className={C(style.svg, { [style.active]: c.facebook })}
					/>
					<Svg.Google className={C(style.svg, { [style.active]: c.google })} />
					<Svg.Twitter
						className={C(style.svg, { [style.active]: c.twitter })}
					/>
					<Svg.Youtube
						className={C(style.svg, { [style.active]: c.youtube })}
					/>
					<Svg.Instagram
						className={C(style.svg, { [style.active]: c.instagram })}
					/>
				</div>
				<p className={style.time}>{time}</p>
				{!draft ? null : <span className={style.draft}>draft</span>}
				<div
					className={style.content}
					onClick={this.onClick}
					onMouseDown={this.onMouseDown}
					onMouseUp={this.onMouseUp}
					onMouseLeave={this.onMouseUp}
				>
					{children}
				</div>
			</div>
		);
	}

	onClick = e => {
		if (this.props.onClick) {
			this.props.onClick(e, this.props.id);
		}
	};

	onMouseDown = () => this.setState({ ispushed: true });

	onMouseUp = () => this.setState({ ispushed: false });
}

Slot.propTypes = {
	id: PropTypes.number.isRequired,
	time: PropTypes.string,
	emptyText: PropTypes.string,
	className: PropTypes.string,
	onClick: PropTypes.func,
	children: PropTypes.any,
	draft: PropTypes.bool,
	checked: PropTypes.shape({
		facebook: PropTypes.bool,
		twitter: PropTypes.bool,
		instagram: PropTypes.bool,
		google: PropTypes.bool,
		youtube: PropTypes.bool,
	}).isRequired,
};
