import React from 'react';
import Day from '../Day/Day';
import PropTypes from 'prop-types';
import Slot from '../Slot/Slot';
import EditorModal from '../Editor/EditorModal';
import dayjs from 'dayjs';

const TODAY = dayjs();

function format(date) {
	const diff = TODAY.diff(date, 'days');
	switch (diff) {
		case 0:
			return 'Today';
		case 1:
			return 'Tomorrow';
		case -1:
			return 'Yesterday';
		default:
			return date.format('MMMM, DD, ddd');
	}
}

export default class SmartDay extends React.Component {
	static defaultProps = {
		date: PropTypes.instanceOf(Date).isRequired,
		slots: PropTypes.arrayOf({
			id: PropTypes.number.isRequired,
			time: PropTypes.string,
			emptyText: PropTypes.string,
			title: PropTypes.string,
			content: PropTypes.string,
			checked: PropTypes.shape({
				facebook: PropTypes.bool,
				twitter: PropTypes.bool,
				instagram: PropTypes.bool,
				google: PropTypes.bool,
				youtube: PropTypes.bool,
			}),
		}).isRequired,
		onApply: PropTypes.func.isRequired,
	};

	state = {
		modalSlot: null,
	};

	render() {
		const { slots, date } = this.props;
		const { modalSlot } = this.state;
		const d = dayjs(date);
		const dayName = format(d);
		const weekDay = d.format('dddd');
		return (
			<Day dayName={dayName} weekDay={weekDay}>
				{slots.map(this.renderSlot)}
				{!modalSlot ? null : (
					<EditorModal
						key="editormodal"
						title={modalSlot.title}
						content={modalSlot.content}
						date={modalSlot.date}
						time={modalSlot.time}
						onClose={this.onCloseModal}
						onApply={this.onApply}
						checked={modalSlot.checked}
					/>
				)}
			</Day>
		);
	}

	onApply = (payload, date, draft) => {
		const slotContent = { ...this.state.modalSlot, ...payload, draft };
		this.props.onApply(date, slotContent);
	};

	onCloseModal = () => {
		this.setState({ modalSlot: null });
	};

	renderSlot = slot => {
		return (
			<Slot
				id={slot.id}
				key={slot.id}
				time={slot.time}
				emptyText={slot.emptyText}
				onClick={this.onClick}
				checked={slot.checked}
				draft={slot.draft}
			>
				{!slot.content ? null : (
					<React.Fragment>
						{!slot.title ? null : (
							<p>
								<strong>{slot.title}</strong>
							</p>
						)}
						{slot.content}
					</React.Fragment>
				)}
			</Slot>
		);
	};

	onClick = (e, id) => {
		const { slots } = this.props;
		const modalSlot = slots.find(it => it.id == id); // eslint-disable-line
		if (!modalSlot) {
			return;
		}
		this.setState({ modalSlot });
	};
}
