# Start project

This test project use yarn as package manager.
For quick start you need install it

```sh
npm install yarn -g
```

Project is based on create-react-app and support all default commands.

```sh
npm start # start dev server
npm run build # build production version of app
```

Also project has been written with `storybook`.
To start ui-kit
```sh
npm run storybook
```
