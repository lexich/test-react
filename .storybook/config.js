import { configure } from '@storybook/react';

function loadStories() {
  require('../src/stories');
}

configure(loadStories, module);

import { configureViewport, INITIAL_VIEWPORTS } from '@storybook/addon-viewport';

configureViewport({
  viewports: {
    ...INITIAL_VIEWPORTS
  }
});
